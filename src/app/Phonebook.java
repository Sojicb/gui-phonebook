package app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import database.DatabaseMethods;

public class Phonebook {

	private LetterNode head;

	/**
	 * This method adds contacts and sorts the list
	 *
	 * @param name        - This parameter holds contacts name
	 * @param lastName    - This parameter holds contacts last name
	 * @param phoneNumber - This parameter holds contacts phone number
	 */
	public void add(String name, String lastName, String phoneNumber) {

		if (head == null) {
			head = new LetterNode(lastName.toUpperCase().charAt(0));
			addToContacts(head, name, lastName, phoneNumber);
			return;
		}

		LetterNode newNode = new LetterNode(lastName.toUpperCase().charAt(0));
		LetterNode delayHead = new LetterNode();
		LetterNode tempHead = head;
		delayHead.setNext(tempHead);

		if (newNode.getLetter() < head.getLetter()) {
			newNode.setNext(head);
			head = newNode;
			addToContacts(newNode, name, lastName, phoneNumber);

		} else {
			while (delayHead.getNext() != null) {
				if (newNode.getLetter() == tempHead.getLetter()) {
					addToContacts(tempHead, name, lastName, phoneNumber);
					return;
				} else if (newNode.getLetter() < tempHead.getLetter()) {
					delayHead.setNext(newNode);
					newNode.setNext(tempHead);
					addToContacts(newNode, name, lastName, phoneNumber);
					return;
				}
				tempHead = tempHead.getNext();
				delayHead = delayHead.getNext();
			}
			delayHead.setNext(newNode);
			addToContacts(newNode, name, lastName, phoneNumber);

		}
	}

	/**
	 * This method, when called, adds contacts information to the new list
	 *
	 * @param contact
	 * @param name
	 * @param lastName
	 * @param phoneNumber
	 */
	private void addToContacts(LetterNode contact, String name, String lastName, String phoneNumber) {

		ContactNode tempHead = contact.getNewList();//head
		ContactNode newContact = new ContactNode(name, lastName, phoneNumber);
		ContactNode delayHead = new ContactNode();


		if (contact.getNewList() == null) {
			contact.setNewList(new ContactNode(name, lastName, phoneNumber));
			ins(newContact);
			return;
		}


		delayHead.setNextContact(tempHead);

		if (newContact.getLastName().compareToIgnoreCase(tempHead.getLastName()) < 0) {
			newContact.setNextContact(tempHead);
			contact.setNewList(newContact);
			ins(newContact);
		}
		//        if (newContact.getLastName().compareToIgnoreCase(tempHead.getLastName()) == 0) {
		//            return;
		// }
		else {
			while (delayHead.getNextContact() != null) {
				if (newContact.getName().compareToIgnoreCase(tempHead.getName()) == 0) {
					return;
				} else if (newContact.getLastName().compareToIgnoreCase(tempHead.getLastName()) < 0) {
					delayHead.setNextContact(newContact);
					ins(newContact);
					newContact.setNextContact(tempHead);
					return;
				}
				tempHead = tempHead.getNextContact();
				delayHead = delayHead.getNextContact();
			}

			delayHead.setNextContact(newContact);
			ins(newContact);

		}

	}

	//TODO add a new column with a letter
	private void ins(ContactNode contactNode) {
		DatabaseMethods dbm = new DatabaseMethods();
		String sql = "insert into contact (name, lastName, number, letter) values (?,?,?,?)";

		try(Connection conn = dbm.connect();
				PreparedStatement ps = conn.prepareStatement(sql)){
			ps.setString(1, contactNode.getName());
			ps.setString(2, contactNode.getLastName());
			ps.setString(3, contactNode.getPhoneNumber());
			ps.setString(4, returnAsChar(contactNode.getLastName()));
			ps.executeUpdate();
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	private String returnAsChar(String lastName) {
		String ret = String.valueOf(lastName.charAt(0));
		return ret;
	}

	/**
	 * Method that deletes a contact by given last name
	 *
	 * @param lastName - parameter that will be used to delete a contact
	 */
	public void delete(String lastName) {

		LetterNode tempNode = head;

		while (tempNode.getNext() != null) {
			if (lastName.toUpperCase().charAt(0) == tempNode.getLetter()) {
				break;
			}
			tempNode = tempNode.getNext();
		}
		ContactNode temp = tempNode.getNewList();
		ContactNode delayTemp = new ContactNode();
		delayTemp.setNextContact(temp);

		if (temp.getLastName().equalsIgnoreCase(lastName)) {
			tempNode.setNewList(temp.getNextContact());
			return;
		}
		while (delayTemp.getNextContact() != null) {
			if (temp.getLastName().equalsIgnoreCase(lastName)) {
				break;
			}
			delayTemp = delayTemp.getNextContact();
			temp = temp.getNextContact();
		}
		delayTemp.setNextContact(temp.getNextContact());

	}

	/**
	 * This method prints a specific section
	 *
	 * @param letter - parameter that holds a letter of a section that will be printed
	 */
	public void printSpecificSection(char letter) {

		LetterNode tempNode = head;
		while (tempNode.getNext() != null) {
			if (letter == tempNode.getLetter()) {
				break;
			}
			tempNode = tempNode.getNext();
		}
		ContactNode tempContact = tempNode.getNewList();

		while (tempContact != null) {
			System.out.print(tempNode.getLetter() + " -> ");
			System.out.print(tempContact.getName() + " " + tempContact.getLastName() + " " + tempContact.getPhoneNumber());
			tempContact = tempContact.getNextContact();
		}
		System.out.println();
	}

	/**
	 * Method that deletes all letters which don't have any contacts
	 */
	private void checkList() {
		LetterNode tempLetter = head;
		LetterNode delayLetter = new LetterNode();
		delayLetter.setNext(tempLetter);

		if (tempLetter.getNewList() == null) {
			head = tempLetter.getNext();
			tempLetter = delayLetter.getNext();
		}

		while (delayLetter.getNext() != null) {
			if (tempLetter.getNewList() == null) {
				delayLetter.setNext(tempLetter.getNext());
				tempLetter = delayLetter.getNext();
			} else {
				tempLetter = tempLetter.getNext();
				delayLetter = delayLetter.getNext();
			}
		}
	}

	/**
	 * This method prints the phone book
	 */
	public void printList() {
		checkList();
		LetterNode tempNode = head;
		ContactNode tempHead;
		if (head == null) {
			System.out.println("List is empty");
			return;
		}
		while (tempNode != null) {
			tempHead = tempNode.getNewList();
			System.out.print(tempNode.getLetter() + " -> ");
			while (tempHead != null) {
				System.out.print(tempHead.getName() + " " + tempHead.getLastName() + " " + tempHead.getPhoneNumber() + ", ");
				tempHead = tempHead.getNextContact();
			}
			System.out.println();
			tempNode = tempNode.getNext();
		}
	}

}
