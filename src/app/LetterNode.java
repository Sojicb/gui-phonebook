package app;

public class LetterNode {


	private char letter;
	private LetterNode next;
	private ContactNode newList;

	public LetterNode() {
	}

	public LetterNode(char letter) {
		this.letter = letter;
	}

	public char getLetter() {
		return letter;
	}

	public void setLetter(char letter) {
		this.letter = letter;
	}

	public LetterNode getNext() {
		return next;
	}

	public void setNext(LetterNode next) {
		this.next = next;
	}

	public ContactNode getNewList() {
		return newList;
	}

	public void setNewList(ContactNode newList) {
		this.newList = newList;
	}

	@Override
	public String toString(){
		return (this.letter + "");
	}

}
