package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Font;
import javax.swing.JList;
import java.awt.Color;

import database.*;

public class MFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	JTextField deleteFieldName;
	JTextField searchField;
	private JTextField deleteFieldLastName;
	DatabaseMethods dbm = new DatabaseMethods();
	DefaultListModel<String> dlm = new DefaultListModel<String>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MFrame frame = new MFrame();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MFrame() {
		initMainFrame();
	}
	
	private void initMainFrame() {
		setTitle("Phonebook");

		CForm cform = new CForm();


		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 452, 657);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnAddContact = new JButton("Add Contact");
		btnAddContact.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cform.setVisible(true);
			}
		});
		btnAddContact.setBounds(10, 36, 115, 23);
		contentPane.add(btnAddContact);

		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MFrame mframe = new MFrame();
				dispatchEvent(new WindowEvent(mframe, WindowEvent.WINDOW_CLOSING));
			}
		});
		btnClose.setBounds(345, 595, 89, 23);
		contentPane.add(btnClose);

		JList<String> displayList = new JList<String>(dlm);
		displayList.setBackground(Color.LIGHT_GRAY);
		displayList.setForeground(Color.BLACK);
		displayList.setBounds(10, 295, 416, 272);
		contentPane.add(displayList);

		deleteFieldName = new JTextField();
		deleteFieldName.setBounds(85, 169, 206, 20);
		contentPane.add(deleteFieldName);
		deleteFieldName.setColumns(10);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AreYouSureFrame sure = new AreYouSureFrame(deleteFieldName.getText(), deleteFieldLastName.getText());
				sure.setVisible(true);
				deleteFieldName.setText(null);
				deleteFieldLastName.setText(null);
			}


		});

		btnDelete.setBounds(337, 183, 89, 23);
		contentPane.add(btnDelete);

		JButton btnPrintList = new JButton("Print List");
		btnPrintList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				printList();
			}
		});
		btnPrintList.setBounds(337, 261, 89, 23);
		contentPane.add(btnPrintList);

		JLabel lblAddANew = new JLabel("Add a New Contact");
		lblAddANew.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAddANew.setBounds(10, 11, 163, 14);
		contentPane.add(lblAddANew);

		JLabel lblSearchAndDelete = new JLabel("Search Contacts by Category (First letter of the Last Name)");
		lblSearchAndDelete.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSearchAndDelete.setBounds(10, 70, 416, 22);
		contentPane.add(lblSearchAndDelete);

		JLabel lblDeleteAContact = new JLabel("Delete Contact");
		lblDeleteAContact.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDeleteAContact.setBounds(10, 134, 105, 24);
		contentPane.add(lblDeleteAContact);

		searchField = new JTextField();
		searchField.setBounds(10, 103, 281, 20);
		contentPane.add(searchField);
		searchField.setColumns(10);

		JButton Connection = new JButton("Connect");
		Connection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connection();
			}	
		});
		Connection.setBounds(345, 0, 89, 23);
		contentPane.add(Connection);

		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				printContacts();
			}
		});
		btnSearch.setBounds(337, 103, 89, 23);
		contentPane.add(btnSearch);

		deleteFieldLastName = new JTextField();
		deleteFieldLastName.setBounds(85, 200, 206, 20);
		contentPane.add(deleteFieldLastName);
		deleteFieldLastName.setColumns(10);

		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 172, 46, 14);
		contentPane.add(lblName);

		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(10, 203, 67, 14);
		contentPane.add(lblLastName);

		JLabel lblListOfContacts = new JLabel("List Of Contacts");
		lblListOfContacts.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblListOfContacts.setBounds(10, 268, 151, 14);
		contentPane.add(lblListOfContacts);
	}
	
	
	/**
	 * Method that prints all contacts by letter
	 */
	public void printContacts() {
		dlm.removeAllElements();
		String sql = "SELECT * FROM contact where letter = ? order by letter, name";
		dlm.addElement("Ime" + "   " + "Prezime" + "   " + "Broj Telefona");
		try (Connection conn = dbm.connect();
				PreparedStatement pstmt  = conn.prepareStatement(sql)){
			pstmt.setString(1, searchField.getText());

			ResultSet rs    = pstmt.executeQuery();
			while (rs.next()) {
				dlm.addElement(rs.getString("name") + "     " + 
						rs.getString("lastName") + "     " +
						rs.getString("number"));
			}
			searchField.setText(null);
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Method that prints all contacts from database.
	 */
	public void printList() {
		dlm.removeAllElements();
		String sql = "SELECT * FROM contact order by lastName, name";
		try (Connection conn = dbm.connect();
				Statement stmt  = conn.createStatement();
				ResultSet rs    = stmt.executeQuery(sql)){

			// loop through the result set
			dlm.addElement("Ime" + "     " + "Prezime" + "     " + "Broj Telefona");
			while (rs.next()) {
				dlm.addElement((rs.getString("name") +  "     " + rs.getString("lastName") + "     " + rs.getString("number")));
				//System.out.println();
			}
		} catch (SQLException e1) {
			System.out.println(e1.getMessage());
		}
	}


	/**
	 * Method that connects the program to the database. (Only needs to be used once).
	 */
	public void connection() {
		Connection conn = null;
		try {
			String url = "jdbc:sqlite:phonebook.db";
			conn = DriverManager.getConnection(url);
			dlm.removeAllElements();
			dlm.addElement("Connection to SQLite has been established.");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
		}
	}
}

