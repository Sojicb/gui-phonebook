package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.DatabaseMethods;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AreYouSureFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private String name;
	private String lastName;


	/**
	 * Launch the application.
	 */
	public void setupConfirmationWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					String tempOne = name;
					String tempTwo = lastName;

					AreYouSureFrame frame = new AreYouSureFrame(tempOne, tempTwo);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public AreYouSureFrame(String name, String lastName) {
		initSureFrame(name, lastName);
	}

	public void initSureFrame(String name, String lastName) {
		setTitle("Are You Sure?");
		this.name = name;
		this.lastName = lastName;
		DatabaseMethods dbm = new DatabaseMethods();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 393, 203);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblAreYouSure = new JLabel("Are you sure you want to delete this contact?");
		lblAreYouSure.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAreYouSure.setBounds(10, 11, 396, 49);
		contentPane.add(lblAreYouSure);

		JButton deleteYes = new JButton("Yes");
		deleteYes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dbm.delete(name, lastName);
				setVisible(false);
				dispose();
			}
		});
		deleteYes.setBounds(95, 93, 89, 23);
		contentPane.add(deleteYes);

		JButton deleteNo = new JButton("No");
		deleteNo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false); 
				dispose();
			}
		});
		deleteNo.setBounds(194, 93, 89, 23);
		contentPane.add(deleteNo);

	}

}
