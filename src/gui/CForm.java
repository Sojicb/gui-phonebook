package gui;

import app.*;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CForm extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel CForm;
	private JTextField nameField;
	private JTextField lastNameField;
	private JTextField numberField;

	/**
	 * Launch the application.
	 */
	public void setupContactForm() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CForm frame = new CForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CForm() {
		initContactForm();
	}

	public void initContactForm() {
		setTitle("Add To Contacts");

		Phonebook phonebook = new Phonebook();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 336, 280);
		CForm = new JPanel();
		CForm.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(CForm);
		CForm.setLayout(null);

		JButton CancleButton = new JButton("Cancel");
		CancleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false); 
				dispose();
			}
		});
		CancleButton.setBounds(231, 218, 89, 23);
		CForm.add(CancleButton);

		JButton AddButton = new JButton("Add Contact");
		AddButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					phonebook.add(nameField.getText(), lastNameField.getText(), numberField.getText());
				}catch(Exception e1){
					//System.out.println(e1.getMessage());
					PopUp pop = new PopUp();
					pop.setVisible(true);
				}
				nameField.setText(null);
				lastNameField.setText(null);
				numberField.setText(null);
				setVisible(false);
				dispose();
			}
		});
		AddButton.setBounds(0, 218, 113, 23);
		CForm.add(AddButton);

		nameField = new JTextField();
		nameField.setBounds(107, 11, 184, 20);
		CForm.add(nameField);
		nameField.setColumns(10);

		lastNameField = new JTextField();
		lastNameField.setBounds(107, 58, 184, 20);
		CForm.add(lastNameField);
		lastNameField.setColumns(10);

		numberField = new JTextField();
		numberField.setBounds(107, 105, 184, 20);
		CForm.add(numberField);
		numberField.setColumns(10);

		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 14, 46, 14);
		CForm.add(lblName);

		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(10, 61, 71, 14);
		CForm.add(lblLastName);

		JLabel lblNumber = new JLabel("Number");
		lblNumber.setBounds(10, 105, 46, 14);
		CForm.add(lblNumber);
	}
}
