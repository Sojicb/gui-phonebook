package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DatabaseMethods {


	/**
	 * Method that is needed for connection to the database.
	 */
	public Connection connect() {
		String url = "jdbc:sqlite:phonebook.db";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return conn;
	}

	/**
	 * Method That deletes a contact from database by name and lastName.
	 * @param name - String containing the name of the contact
	 * @param lastName - String Containing the last name of the contact
	 */
	public void delete(String name, String lastName) {
		String sql = "delete from contact where name = ? and lastName = ?";

		try(Connection conn = this.connect();
				PreparedStatement ps = conn.prepareStatement(sql)){
			ps.setString(1, name);
			ps.setString(2, lastName);
			ps.executeUpdate();
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
	}
}
